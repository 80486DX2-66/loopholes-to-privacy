# Loopholes To Privacy

## Table of contents

1. [Introduction](#introduction)
2. [Attacks](#attacks)
   1. [TEMPEST](#tempest)
   2. [Van Eck phreaking](#van-eck-phreaking)
   3. [Evil maid attack](#evil-maid-attack)
   4. [Cold boot attack](#cold-boot-attack)

## Introduction

While phishing and malware aim to steal data, lesser-known issues create
loopholes that let bad actors covertly access data and monitor users. Since
many people are unaware of these overlooked privacy vulnerabilities,
understanding how they work empowers us to defend ourselves through simple
security practices, advocating for companies to make more secure devices, and
championing digital privacy as a fundamental right. By learning about chip
backdoors, attacks exploiting minor flaws, and bugs causing unintended data
leaks, we can minimize unwanted exposure of our personal information and
advocate for a more secure future.

## Attacks

### TEMPEST

[Wikipedia](https://en.wikipedia.org/wiki/Tempest_(codename)) *@ UTC 05:54, 26 June 2023*:
> **TEMPEST** is a U.S. National Security Agency specification and a NATO
> certification referring to spying on information systems through leaking
> emanations, including unintentional radio or electrical signals, sounds, and
> vibrations. TEMPEST covers both methods to spy upon others and how to shield
> equipment against such spying. The protection efforts are also known as
> emission security (EMSEC), which is a subset of communications security
> (COMSEC).

Purchase a pre-made TEMPEST-rated Faraday cage to fully enclose your computer
and monitor. This will block virtually all electromagnetic emissions from your
devices. You can also construct your own Faraday cage using conductive mesh,
sheeting or foam.

Apply special anti-electromagnetic shield paint to the inside surfaces of
walls, ceilings and floors. This conductive coating can help block
electromagnetic signals from escaping a room.

Disable any wireless connections, radios and antennas when working with
sensitive data to prevent electromagnetic leaks from those sources. This
includes turning off WiFi, Bluetooth and mobile data on devices you are using.

Investing in professionally engineered defenses like Faraday enclosures and
shield paint offers the strongest protection against electromagnetic data leaks
susceptible to van Eck phreaking. But also following the simple habit of
disconnecting from wireless networks when handling sensitive information can
further reduce your vulnerability.

### Van Eck phreaking

[Wikipedia](https://en.wikipedia.org/wiki/Van_Eck_phreaking) *@ UTC 20:05, 20 June 2023*:
> **Van Eck phreaking**, also known as **Van Eck radiation**, is a form of
> eavesdropping in which special equipment is used to pick up side-band
> electromagnetic emissions from electronic devices that correlate to hidden
> signals or data to recreate these signals or data to spy on the electronic
> device. Side-band electromagnetic radiation emissions are present in (and
> with the proper equipment, can be captured from) keyboards, computer
> displays, printers, and other electronic devices.

Cover the screens of devices like computers and phones when not in use with
opaque covers to block visual data leaks from monitors and screens.

Move devices that handle sensitive data away from windows to limit the
transmission of electromagnetic signals that an adversary could intercept.
Placing devices on inner walls or in enclosed spaces can provide additional
shielding.

Where possible, fully power down devices like computers and phones after each
use. This can prevent data leaks from idle but turned on devices.

Making covering screens a routine, positioning devices thoughtfully, and
powering down equipment between uses are low-tech approaches that offer some
level of protection against van Eck phreaking attacks seeking to exploit visual
or electromagnetic emissions.

Taking these simple precautions after each use - covering screens, relocating
devices, powering down equipment - can collectively reduce your vulnerability
to van Eck phreaking despite any technological safeguards.

### Evil maid attack

[Wikipedia](https://en.wikipedia.org/wiki/Evil_maid_attack) *@ UTC 21:18, 9
July 2023*:
> An **evil maid attack** is an attack on an unattended device, in which an
> attacker with physical access alters it in some undetectable way so that they
> can later access the device, or the data on it.

Use full disk encryption software like VeraCrypt to encrypt the entire contents
of your disk drive. This prevents unauthorized access to data even if an
attacker gains physical access to the disk drive.

With full disk encryption, an attacker cannot read or modify the data directly.
They can only replace the entire encrypted disk or corrupt it, rendering the
encrypted data unreadable without the encryption key.

Full disk encryption effectively defeats attacks that rely on gaining physical
access to bypass other security layers. As long as your encryption key and
password remain secure, your data remains protected, even if an adversary
physically steals or tampers with your hard drive.

### Cold boot attack

[Wikipedia](https://en.wikipedia.org/wiki/Cold_boot_attack) *@ UTC 15:34, 7
July 2023*:
> In computer security, a **cold boot attack** (or to a lesser extent, a
> **platform reset attack**) is a type of side channel attack in which an
> attacker with physical access to a computer performs a memory dump of a
> computer's random-access memory (RAM) by performing a hard reset of the
> target machine. Typically, cold boot attacks are used for retrieving
> encryption keys from a running operating system for malicious or criminal
> investigative reasons. The attack relies on the data remanence property of
> DRAM and SRAM to retrieve memory contents that remain readable in the seconds
> to minutes following a power switch-off.

Look for computers and laptops with a feature that automatically wipes or
clears RAM on shutdown. This ensures any data in RAM is erased before an
attacker could access it.

In computers without this feature:
- Disable sleep mode, hibernation and fast startup options and fully power down
  your device after use. This will clear the RAM and make any retained data
  inaccessible.
- Shortly after powering off, physically remove the power cord to cut power to
  the RAM. This can help accelerate data decay in the RAM chips over time.

While RAM-clearing hardware offers the best protection, following the software
and physical steps above after each use can also help mitigate the risk of RAM
extraction, due to data decay and overwriting of RAM locations over time.

<!-- document version: 0.1 alpha -->
